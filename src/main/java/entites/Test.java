/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author DJODJI Kangni Jean-pierre C.
 */
public class Test {
    public static void main(String[] args) {
        //Categories
        Categorie boissons = new Categorie(345, "Boissons", "VenteDeBoisson");
        Categorie alimentation = new Categorie(586, "Alimentation", "VenteDeAlimentation");

        //dates
        LocalDate date1 = LocalDate.of(2021, 2, 18);
        LocalDate date2 = LocalDate.of(2021, 5, 11);

        //produit
        Produit boisson1 = new Produit(145, "boisson1", 200, date1, boissons);
        Produit boisson2 = new Produit(200, "boisson2", 500, date2, boissons);
        Produit alimentaion1 = new Produit(351, "alimentation1", 1500, date1, alimentation);
        Produit alimentation2 = new Produit(252, "alimentation2", 1700, date2, alimentation);

        //Produit achete
        ProduitAchete cocktail = new ProduitAchete(boisson1, 4, 0.5);
        ProduitAchete cocacola = new ProduitAchete(boisson2, 2, 0.3);
        ProduitAchete riz = new ProduitAchete(alimentation1, 3, 0.75);
        ProduitAchete tomate = new ProduitAchete(alimentation2, 5, 0.25);
        
        //produit
        ArrayList<ProduitAchete> array1 = new ArrayList<ProduitAchete>();
        array1.add(tilapia);
        array1.add(carpe);
        array1.add(poulet);
        array1.add(ailes);
        
        //Achat
        Achat achat1 = new Achat(106, 0.60, date1, array1);
        //Result
        System.out.println(achat1.toString());
        System.out.println(achat1.getPrixTotal()); 
    }
}
